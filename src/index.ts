import express, { Request, Response } from "express";
import { graphqlHTTP } from "express-graphql";
import { selectTeamQuery } from "./schema";

const app = express();

app.use(
  "/graphQl",
  graphqlHTTP({
    schema: selectTeamQuery(),
    graphiql: true,
  })
);

app.listen(4000, () => {
  console.log("App listening on port 4000");
});
