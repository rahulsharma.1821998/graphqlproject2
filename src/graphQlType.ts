import { GraphQLObjectType, GraphQLInt, GraphQLString } from "graphql";

export const Team = new GraphQLObjectType({
  name: "Team",
  extensions: { joinMonster: { sqlTable: "team", uniqueKey: "id" } },
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
  }), //()=>({}) this syntax used when we want to only return one statement as Object
});
