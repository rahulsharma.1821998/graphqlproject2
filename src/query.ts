import { connection } from "./dbHelper";
import {
  GraphQLInt,
  GraphQLList,
  GraphQLFieldResolver,
  GraphQLNonNull,
} from "graphql";
import joinMonster from "join-monster";
import { Team } from "./graphQlType";
import { connect } from "./schema";

type argsType = {
  [column: string]: number;
};

export const displayAllTeam = {
  type: new GraphQLList(Team),
  resolve: ((parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, {}, async (sqlTable: string) => {
      return connect.query(sqlTable);
    });
  }) as GraphQLFieldResolver<any, any, any>,
};
export const displayOneTeam = {
  type: new GraphQLList(Team),
  args: { id: { type: GraphQLNonNull(GraphQLInt) } },
  extensions: {
    joinMonster: {
      where: (teamTable: string, args: argsType, context: {}) => {
        return `id = ${args.id}`;
      },
    },
  },
  resolve: ((parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, {}, async (sqlTable: string) => {
      return connect.query(sqlTable);
    });
  }) as GraphQLFieldResolver<any, any, any>,
};
