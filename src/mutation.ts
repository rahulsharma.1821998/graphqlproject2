import {
  GraphQLInt,
  GraphQLString,
  GraphQLList,
  GraphQLFieldResolver,
  GraphQLNonNull,
} from "graphql";
import joinMonster from "join-monster";
import { Team } from "./graphQlType"
import { connect } from "./schema";

type argsType = {
  [column: string]: number;
};

export const addTeam = {
  type: Team,
  args: {
    name: { type: GraphQLNonNull(GraphQLString) },
  },
  resolve: (async (parent, args, context, resolveInfo) => {
    try {
      return (
        await connect.query("INSERT INTO team (name) VALUES ($1) RETURNING *", [
          args.name,
        ])
      ).rows[0];
    } catch (err) {
      throw new Error("Failed to insert new player");
    }
  }) as GraphQLFieldResolver<any, any, any>,
};
export const deleteTeam = {
  type: Team,
  args: { id: { type: GraphQLNonNull(GraphQLInt) } },
  where: (teamTable: string, args: argsType, context: {}) => {
    return `${teamTable}.id = ${args.id}`;
  },
  resolve: (async (parent, args: argsType, context, resolveInfo) => {
    try {
      return (
        await connect.query("DELETE FROM team where id = $1 RETURNING *", [
          args.id,
        ])
      ).rows[0];
    } catch (err) {
      throw new Error("Failed to delete new player");
    }
  }) as GraphQLFieldResolver<any, any, any>,
};
export const updateTeam = {
  type: Team,
  args: {
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
  },
  where: (teamTable: string, args: argsType, context: {}) => {
    return `${teamTable}.id = ${args.id}`;
  },
  resolve: (async (parent, args: argsType, context, resolveInfo) => {
    try {
      return (
        await connect.query(
          "UPDATE team SET name = $2 WHERE id = $1 RETURING *",
          [args.id, args.name]
        )
      ).rows[0];
    } catch (err) {
      throw new Error("Failed to updated new player");
    }
  }) as GraphQLFieldResolver<any, any, any>,
};
export const loginTeam = {
  type: new GraphQLList(Team),
  args: {
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
  },
  extensions: {
    joinMonster: {
      where: (teamTable: string, args: argsType, context: {}) => {
        return `id = ${args.id} and name = '${args.name}'  `;
      },
    },
  },
  resolve: ((parent, args, context, resolveInfo) => {
    return joinMonster(resolveInfo, {}, async (sqlTable: string) => {
      console.log(sqlTable);

      return connect.query(sqlTable);
    });
  }) as GraphQLFieldResolver<any, any, any>,
};
