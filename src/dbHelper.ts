import { Client } from "pg";

const HOST = "localhost";
const PORT_NO = 5432;
const USERNAME = "postgres";
const PASSWORD = "root";
const DB_NAME = "league";
let connection : Client 

const client = new Client({
  host: HOST,
  port: PORT_NO,
  user: USERNAME,
  password: PASSWORD,
  database: DB_NAME,
});
client.connect();
connection = client;
export { connection } ;
