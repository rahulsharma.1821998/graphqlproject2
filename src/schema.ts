import { connection } from "./dbHelper";
import { GraphQLObjectType, GraphQLString, GraphQLSchema } from "graphql";
import { Client } from "pg";
import { addTeam, deleteTeam, updateTeam, loginTeam } from "./mutation";
import { displayAllTeam, displayOneTeam } from "./query";

export let connect = connection;

export function selectTeamQuery(): GraphQLSchema {
  const mutationRoot = new GraphQLObjectType({
    name: "Mutation",
    fields: () => ({
      addTeam: addTeam,
      deleteTeam: deleteTeam,
      updateTeam: updateTeam,
      loginTeam: loginTeam,
    }),
  });

  const queryRoot = new GraphQLObjectType({
    name: "Query",
    fields: () => ({
      hello: {
        type: GraphQLString,
        resolve: () => "Hello world!",
      },
      teams: displayAllTeam,
      displayOneTeam: displayOneTeam,
    }),
  });
  return new GraphQLSchema({ query: queryRoot, mutation: mutationRoot });
}
